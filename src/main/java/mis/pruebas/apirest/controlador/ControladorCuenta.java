package mis.pruebas.apirest.controlador;


import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(Rutas.CUENTAS)
public class ControladorCuenta {

    @Autowired
    ServicioCuenta servicioCuenta;


    @GetMapping
    public List<Cuenta> obtenerCuentas(){ return this.servicioCuenta.obtenerCuentas();}


    @PostMapping
    public void agregarCuenta(@RequestBody Cuenta cuenta){this.servicioCuenta.insertarCuentaNueva(cuenta);}


    @GetMapping("/{numero}")
    public Cuenta obtenerunaCuenta(@PathVariable String numero) {
        return this.servicioCuenta.obtenerCuenta(numero);
    }


    @PutMapping("/{numero}")
    public void reemplazarunaCuenta(@PathVariable("numero") String ctanumero,
                                    @RequestBody Cuenta cuenta){
        cuenta.numero = ctanumero;
        this.servicioCuenta.guardarCuenta(cuenta);

    }


    @PatchMapping("/{numero}")
    public void emparcharunaCuenta(@PathVariable("numero") String ctanumero,
                                    @RequestBody Cuenta cuenta) {
        cuenta.numero = ctanumero;
        this.servicioCuenta.emparcharCuenta(cuenta);
    }


    @DeleteMapping("/{numero}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarunaCuenta(@PathVariable String numero) {
        try{
            this.servicioCuenta.borrarCuenta(numero);
        } catch (Exception x) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

}
